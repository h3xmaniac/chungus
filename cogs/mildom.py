from discord.ext import commands
import discord
import mildom
import os

OUR_USER = int(os.getenv("LIVE_USER"))
OUR_CHANNEL = int(os.getenv("DISCORD_CHANNEL"))


class Mildom(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    async def poll_live(self, ctx):
        pass

    async def build_embed(self):
        user = mildom.User(OUR_USER)  # i want to async this >:(
        name = user.name
        avatar = user.avatar_url
        latest_stream_name = user.latest_live_title
        latest_thumbnail = user.latest_live_thumbnail
        playback = user.fetch_playback(index=0)
        latest_stream_url = playback.url

        embed = discord.Embed(
            title=latest_stream_name,
            url=latest_stream_url,
            color=0x07D7F2,
        )
        embed.set_author(name=name + "🎥🔴", icon_url=avatar)
        embed.set_thumbnail(url=latest_thumbnail)
        return embed


def setup(bot):
    bot.add_cog(Mildom(bot))
