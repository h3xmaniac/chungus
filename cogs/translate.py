import asyncio
from discord.ext import commands
from itranslate import atranslate as atrans

# https://github.com/ffreemt/google-itranslate
from enchant.checker import SpellChecker

# https://pyenchant.github.io/pyenchant


class Translate(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.chkr = SpellChecker("en_US")  # intiliaze spellchecker

    async def do_translate(self, text):
        result = await atrans(
            text,
            to_lang="en",  # make everything english
            url="https://translate.google.com",  # this module uses google.cn by default. we cant have that can we?
        )
        # print(result)
        return str(result)

    @commands.command(hidden=True)
    async def transmsg(self, ctx, *, msg: str):
        # print(msg)
        result = await self.do_translate(msg)
        await ctx.send(result)

    @transmsg.error
    async def handle_error(self, ctx, error):
        if isinstance(error, commands.errors.MissingRequiredArgument):
            await ctx.send(
                "look, i know this command is for debug purposes..but that doesnt mean you can send blank messages.."
            )

    @commands.command(hidden=True)
    async def checkmsg(self, ctx, *, msg: str):
        self.chkr.set_text(msg)
        fail = len([err.word for err in self.chkr])  # i hate this lmao
        await ctx.send(f"{fail} error(s)! Wow your english is worse than mine!")  # kekw


def setup(bot):
    bot.add_cog(Translate(bot))
