Chungus
<a href="https://python.org"> <img alt="Get Python" src=https://img.shields.io/badge/python-3.9.6-blue> <a href="https://github.com/psf/black"><img alt="Code style: black" src="https://img.shields.io/badge/code%20style-black-black">
<a href="https://gitlab.com/h3xmaniac/chungus/-/blob/master/LICENSE"> <img alt="Mozilla Public License" src=https://img.shields.io/badge/license-MPL-orange>
=========

![chungus](https://i.redd.it/owk63zkbfzf61.jpg)

<br>

*Konnichiwassup watashi my oni-chan!? Genki how are you desuka? 海外ニキ*




1. Install [pipenv](https://pipenv.pypa.io/en/latest/)
2. Edit [sample-env](https://gitlab.com/h3xmaniac/chungus/-/blob/master/sample-env) and rename to `.env`
3. `pipenv update`
4. `pipenv run python bot.py`
