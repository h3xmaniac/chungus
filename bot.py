import discord
from discord.ext import commands
import traceback
import os
import platform

TOKEN = os.getenv("TOKEN")

if platform.system().startswith("Linux"):
    import uvloop

    uvloop.install()  # uvloop go faaaaaaaaaaaaast


initial_extensions = [  # cogs to load
    "cogs.allmessages",
    "cogs.translate",
    "cogs.mildom",
    "jishaku",
]

# invite url
# https://discordapp.com/oauth2/authorize?client_id=862114688720699403&scope=bot&permissions=2147559488
# Equation: 2147559488 = 0x40 | 0x800 | 0x2000 | 0x10000 | 0x80000000
# send messages | add reactions | manage messages | read message history | use slash commands


class Bot(commands.Bot):
    async def on_ready(self):
        print("------")
        print(f"I am  {bot.user.name} ({bot.user.id})")
        print("------")

        if __name__ == "__main__":
            for extension in initial_extensions:
                try:
                    bot.load_extension(extension)  # actaully load cogs
                except Exception as e:
                    print("Failed to load extension" + str(extension))
                    traceback.print_exc()

        await bot.change_presence(
            activity=discord.Activity(
                type=discord.ActivityType.listening,
                name="Weather Hackers",  # i miss her
            )
        )


bot = Bot(command_prefix="c!")  # initialize bot class


@bot.command()  # test command
async def ping(ctx):
    png = str(round(bot.latency * 1000, 2))
    return await ctx.send("Pong! `" + png + "ms` :ping_pong:")


bot.run(TOKEN)  # run the bot
